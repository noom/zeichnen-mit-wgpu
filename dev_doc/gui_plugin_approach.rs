pub type GuiFn = Box<dyn FnOnce(f32, &imgui::Ui)>;

struct Gui {
    imgui: imgui::Context,
    platform: imgui_winit_support::WinitPlatform,
    renderer: Renderer,
    callback: Option<GuiFn>,
}

impl Gui {
    fn render<'a>(
        &'a mut self,
        mut rpass: wgpu::RenderPass<'a>,
        queue: &wgpu::Queue,
        device: &wgpu::Device,
        window: &crate::window::Window,
    ) -> Result<(), PluginError> {
        if let Some(callback) = self.callback.take() {
            let framerate = self.imgui.io().framerate;

            self.platform
                .prepare_frame(self.imgui.io_mut(), window)
                .map_err(|e| PluginError::RenderFail {
                    msg: format!("Error preparing imgui frame: {}", e),
                })?;

            let ui = self.imgui.frame();
            callback(framerate, &ui);
            let draw_data = ui.render();
            self.renderer
                .render(draw_data, queue, device, &mut rpass)
                .map_err(|e| PluginError::RenderFail {
                    msg: format!("Error rendering imgui frame: {:?}", e),
                })?;
        }
        Ok(())
    }

    fn process_event(&mut self, window: &window::Window, event: &window::WEvent<()>) -> bool {
        let io = self.imgui.io_mut();
        self.platform.handle_event(io, &window, event);
        io.want_capture_mouse
    }
}

#[derive(Clone)]
pub struct ImguiPlugin(Arc<Mutex<Gui>>);

impl ImguiPlugin {
    pub fn prepare(&self, callback: GuiFn) {
        let mut inner = self.0.lock().unwrap();
        inner.callback = Some(callback);
    }
}

impl Plugin for ImguiPlugin {
    /// Initialize a new GUI instance
    fn init(
        wd: &WindowData,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        hidpi_factor: f32,
        window: &crate::window::Window,
    ) -> Self {
        // Set up dear imgui﻿cwfitzgerald: 
        let mut imgui = imgui::Context::create();
        let mut platform = imgui_winit_support::WinitPlatform::init(&mut imgui);
        platform.attach_window(
            imgui.io_mut(),
            window,
            imgui_winit_support::HiDpiMode::Default,
        );
        imgui.set_ini_filename(None);

        let font_size = (13.0 * hidpi_factor) as f32;
        imgui.io_mut().font_global_scale = (1.0 / hidpi_factor) as f32;

        imgui.fonts().add_font(&[FontSource::DefaultFontData {
            config: Some(imgui::FontConfig {
                oversample_h: 1,
                pixel_snap_h: true,
                size_pixels: font_size,
                ..Default::default()
            }),
        }]);

        // We don't pass a clear otherwise it ovewrites everything we wrote
        let renderer = Renderer::new(&mut imgui, device, queue, wd.format());

        ImguiPlugin(Arc::new(Mutex::new(Gui {
            imgui,
            platform,
            renderer,
            callback: None,
        })))
    }

    /// Send the events into imgui; returns whether it captured them (when mouse is over window
    fn process_event(&self, window: &window::Window, event: &window::WEvent<()>) -> bool {
        let mut gui = self.0.lock().unwrap();
        gui.process_event(window, event)
    }

    fn render(
        &self,
        rpass: wgpu::RenderPass,
        queue: &wgpu::Queue,
        device: &wgpu::Device,
        window: &crate::window::Window,
    ) -> Result<(), PluginError> {
        if let Ok(mut gui) = self.0.lock() {
            gui.render(rpass, queue, device, window)?;
        }
        Ok(())
    }
}

