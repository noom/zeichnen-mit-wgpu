use winit::{dpi::PhysicalSize, window::Window};

use wgpu::*;

use crate::TEXTURE_FORMAT;

pub struct WindowContext {
    pub window: Window,

    pub surface: Surface,
    pub sc_desc: SwapChainDescriptor,
    pub swap_chain: SwapChain,
}

impl WindowContext {
    pub fn new(instance: &Instance, window: Window, device: &Device) -> Self {
        let size = window.inner_size();

        let surface = unsafe { instance.create_surface(&window) };

        let sc_desc = SwapChainDescriptor {
            usage: TextureUsage::OUTPUT_ATTACHMENT,
            format: TEXTURE_FORMAT,
            width: size.width,
            height: size.height,
            present_mode: PresentMode::Fifo,
        };
        let swap_chain = device.create_swap_chain(&surface, &sc_desc);

        Self {
            window,

            surface,
            sc_desc,
            swap_chain,
        }
    }

    pub fn resize_window(&mut self, device: &Device, size: &PhysicalSize<u32>) {
        self.sc_desc.width = size.width;
        self.sc_desc.height = size.height;
        self.swap_chain = device.create_swap_chain(&self.surface, &self.sc_desc);
    }

    pub fn request_redraw(&mut self) {
        // RedrawRequested will only trigger once, unless we manually
        // request it.
        self.window.request_redraw();
    }

    /// Return SwapChainFrame which has to be kept alive until the queue has been submitted.
    pub fn begin_render(
        &mut self,
        device: &Device,
        queue: &Queue,
        encoder: &mut CommandEncoder,
    ) -> Option<SwapChainFrame> {
        let frame = if let Ok(frame) = self.swap_chain.get_current_frame() {
            frame
        } else {
            return None;
        };

        // Clear the screen
        encoder.begin_render_pass(&RenderPassDescriptor {
            color_attachments: &[RenderPassColorAttachmentDescriptor {
                attachment: &frame.output.view,
                resolve_target: None,
                ops: Operations {
                    load: LoadOp::Clear(Color::BLACK),
                    store: true,
                },
            }],
            depth_stencil_attachment: None,
        });

        // Keeping frame alive to avoid presenting two times when submitting the queue
        Some(frame)
    }
}
