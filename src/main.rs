use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use wgpu::TextureFormat;

use futures::executor::block_on;

use structopt::StructOpt;

use log::info;

use env_logger::Env;

use std::io::Write;

mod application;

mod context;
mod pipelines;
mod shader_context;
mod window_context;

mod ui_context;
mod ui_controls;
mod ui_style;

mod utils;

mod project;

use application::Application;
use project::Project;
use shader_context::ShaderContext;

enum PresentingMode {
    Realtime,
    Headless,
}

const DEFAULT_VERT: &'static str = include_str!("shader.vert");
const QUAD_TEXTURE_VERT: &'static str = include_str!("quad_texture.vert");
const QUAD_TEXTURE_FRAG: &'static str = include_str!("quad_texture.frag");

const PRESENTING_MODE: PresentingMode = PresentingMode::Realtime;
// const HEADLESS_SIZE: winit::dpi::PhysicalSize<u32> = winit::dpi::PhysicalSize::new(1920, 1080);
const HEADLESS_SIZE: winit::dpi::PhysicalSize<u32> = winit::dpi::PhysicalSize::new(950, 518);
const TEXTURE_FORMAT: TextureFormat = TextureFormat::Bgra8UnormSrgb;

#[derive(StructOpt, Debug)]
#[structopt(name = "Zeichnen Mit Wgpu")]
struct Opt {
    /// Number of frame to generate
    // #[structopt(short, long, default_value = "1")]
    // frame_count: u32,

    /// Duration of the render in seconds
    #[structopt(short, long, default_value = "1")]
    duration: u32,

    /// Number of frames per second
    #[structopt(short, long, default_value = "30")]
    frame_rate: u32,
}

fn main() {
    let opt = Opt::from_args();

    // env_logger::init_from_env(env_logger::Env::from("RUST_LOG=info"));
    env_logger::from_env(Env::default().default_filter_or("info")).init();
    // wgpu_subscriber::initialize_default_subscriber(None);

    info!("Zelcome to Zei!");

    match PRESENTING_MODE {
        PresentingMode::Realtime => {
            let event_loop = EventLoop::new();

            let window = WindowBuilder::new().build(&event_loop).unwrap();
            let main_window_id = window.id();

            let mut app = block_on(Application::new(opt.frame_rate, Some(window)));

            event_loop.run(move |event, _, control_flow| {
                *control_flow = ControlFlow::Poll;

                match event {
                    Event::WindowEvent {
                        ref event,
                        window_id,
                    } => {
                        if window_id == main_window_id {
                            match event {
                                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                                // WindowEvent::ModifiersChanged(modifiers_state) => {}
                                WindowEvent::CursorMoved { position, .. } => {
                                    app.cursor_moved(position)
                                }
                                WindowEvent::KeyboardInput { input, .. } => match input {
                                    KeyboardInput {
                                        state: ElementState::Pressed,
                                        virtual_keycode: Some(VirtualKeyCode::Escape),
                                        ..
                                    } => (),
                                    _ => {}
                                },
                                WindowEvent::Resized(physical_size) => {
                                    app.resized(physical_size);
                                }
                                WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                                    app.scale_factor_changed(*new_inner_size);
                                }
                                _ => {}
                            }
                        }
                    }
                    Event::RedrawRequested(_) => {
                        // state.redraw_requested();
                    }
                    Event::MainEventsCleared => {
                        app.main_events_cleared();
                    }
                    Event::RedrawEventsCleared => {
                        app.redraw_events_cleared();
                    }
                    _ => {}
                }

                app.event(event);
            });
        }
        PresentingMode::Headless => {
            let mut app = block_on(Application::new(opt.frame_rate, None));
            let mut perc = 0.;

            let frame_count = opt.duration * opt.frame_rate;

            for i in 0..frame_count {
                let new_perc = (i as f32 + 2.) as f32 / frame_count as f32 * 100.;
                if new_perc != perc {
                    print!("rendering {:.0}%\r", perc);
                    std::io::stdout().flush().unwrap();
                    perc = new_perc;
                }

                app.take_capture = true;

                app.update();
                app.render();

                app.take_capture = false;
                block_on(app.create_png(&format!("CAPTURE_{:07}.png", i + 1)));
            }
        }
    }
}
