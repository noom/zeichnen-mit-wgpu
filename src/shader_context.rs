use wgpu::*;

use winit::dpi::{PhysicalPosition, PhysicalSize};

use glsl_layout::{self as glsl, as_bytes, AsStd140};

use log::info;

use notify::{
    event::{CreateKind, DataChange, EventKind, ModifyKind},
    Event, RecommendedWatcher, RecursiveMode, Watcher,
};

use std::{
    path::{Path, PathBuf},
    sync::mpsc::{channel, Receiver, Sender},
    time::{Duration, Instant},
};

use crate::{
    pipelines::{make_shader_pipeline, Pipeline},
    project::{Fields, Project, Resource},
    PresentingMode, PRESENTING_MODE, TEXTURE_FORMAT,
};

#[repr(C)]
#[derive(Debug, Default, Copy, Clone, AsStd140)]
pub struct FragmentUniforms {
    pub time: glsl::float,
    pub mouse_position: glsl::vec2,
    pub resolution: glsl::vec2,
}

// TODO: remove me (function is already implemented as `glsl_layout::as_bytes`)
// pub fn bytes_from<T>(t: &T) -> &[u8]
// where
//     T: Copy + Sized,
// {
//     unsafe fn bytes_from<T>(t: &T) -> &[u8]
//     where
//         T: Copy + Sized,
//     {
//         let len = std::mem::size_of::<T>();
//         let ptr = t as *const T as *const u8;
//         std::slice::from_raw_parts(ptr, len)
//     }

//     unsafe { bytes_from(t) }
// }

struct ShaderState {
    initial_time: Instant,
    time: Duration,

    mouse_position: [f32; 2],
}

struct ShaderWatcher {
    watcher: RecommendedWatcher,
    sender: Sender<notify::Event>,
    receiver: Receiver<notify::Event>,
}

pub struct ShaderContext {
    frame_rate: u32,
    state: ShaderState,

    offscreen_texture: Texture,
    offscreen_view: TextureView,

    pipeline: Pipeline,

    watcher: ShaderWatcher,
}

impl ShaderContext {
    pub fn new(
        device: &Device,
        frame_rate: u32,
        // shader_path: impl AsRef<Path>,
        frag_shader_res: &Resource,
        resolution: &[u32; 2],
    ) -> Self {
        let state = ShaderState {
            initial_time: Instant::now(),
            time: Duration::default(),
            mouse_position: [0.; 2],
        };

        let offscreen_texture = device.create_texture(&TextureDescriptor {
            label: Some("Offscreen texture"),
            size: Extent3d {
                width: resolution[0],
                height: resolution[1],
                depth: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TEXTURE_FORMAT,
            usage: TextureUsage::COPY_DST
                | TextureUsage::COPY_SRC
                | TextureUsage::OUTPUT_ATTACHMENT
                | TextureUsage::SAMPLED,
        });

        let offscreen_view = offscreen_texture.create_view(&TextureViewDescriptor::default());

        let pipeline = make_shader_pipeline(&device, frag_shader_res.content());

        let (sender, receiver) = channel::<notify::Event>();

        let mut watcher: RecommendedWatcher = {
            let sender = sender.clone();
            Watcher::new_immediate(move |res| match res {
                Ok(event) => {
                    sender.send(event).unwrap();
                }
                Err(e) => panic!(e),
            })
            .unwrap()
        };

        // TODO: uncomment this
        // watcher
        //     .watch(
        //         shader_path.as_ref().parent().unwrap(),
        //         RecursiveMode::Recursive,
        //     )
        //     .expect("Could not watch shader file");

        let watcher = ShaderWatcher {
            watcher,
            sender,
            receiver,
        };

        Self {
            frame_rate,
            state,
            offscreen_texture,
            offscreen_view,
            pipeline,
            watcher,
        }
    }

    pub fn texture(&self) -> &Texture {
        &self.offscreen_texture
    }

    pub fn view(&self) -> &TextureView {
        &self.offscreen_view
    }

    pub fn change_shader(&mut self, device: &Device, frag_shader_res: &Resource) {
        self.pipeline = make_shader_pipeline(device, frag_shader_res.content());
    }

    pub fn resize(&mut self, device: &Device, size: &[u32; 2]) {
        let offscreen_texture = device.create_texture(&wgpu::TextureDescriptor {
            label: Some("Offscreen texture"),
            size: wgpu::Extent3d {
                width: size[0],
                height: size[1],
                depth: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: TEXTURE_FORMAT,
            usage: wgpu::TextureUsage::COPY_DST
                | wgpu::TextureUsage::COPY_SRC
                | wgpu::TextureUsage::OUTPUT_ATTACHMENT
                | wgpu::TextureUsage::SAMPLED,
        });
        let offscreen_texture_view =
            offscreen_texture.create_view(&wgpu::TextureViewDescriptor::default());

        self.offscreen_texture = offscreen_texture;
        self.offscreen_view = offscreen_texture_view;
    }

    pub fn update_cursor_position(
        &mut self,
        size: &PhysicalSize<u32>,
        position: &PhysicalPosition<f64>,
    ) {
        self.state.mouse_position[0] = position.x as f32 / size.width as f32;
        self.state.mouse_position[1] = position.y as f32 / size.height as f32;
    }

    pub fn update(&mut self, project: &Project) {
        if let Ok(event) = self.watcher.receiver.try_recv() {
            match event.kind {
                EventKind::Modify(ModifyKind::Data(DataChange::Content))
                | EventKind::Create(CreateKind::File) => {
                    for path in event.paths {
                        // TODO: check if path == project.info.path
                        todo!();
                    }
                }
                _ => (),
            }
        }
    }

    pub fn render(&mut self, project: &Project, device: &Device, queue: &Queue) {
        self.update(project);

        // TODO: should I update time in an `update` function?
        self.state.time = match PRESENTING_MODE {
            PresentingMode::Realtime => Instant::now().duration_since(self.state.initial_time),
            PresentingMode::Headless => {
                self.state.time + Duration::from_secs_f32(1. / self.frame_rate as f32)
            }
        };

        let uniforms = FragmentUniforms {
            time: self.state.time.as_secs_f32(),
            mouse_position: self.state.mouse_position.into(),
            resolution: [
                project.fields.resolution[0] as f32,
                project.fields.resolution[1] as f32,
            ]
            .into(),
        }
        .std140();
        queue.write_buffer(
            self.pipeline.fragment_uniforms_buffer.as_ref().unwrap(),
            0,
            as_bytes(&uniforms),
        );

        let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("Render Encoder"),
        });

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: &self.offscreen_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.pipeline.render_pipeline);

            render_pass.set_bind_group(0, &self.pipeline.bind_group, &[]);

            render_pass.set_vertex_buffer(0, self.pipeline.vertex_buffer.slice(..));
            render_pass.set_index_buffer(self.pipeline.index_buffer.slice(..));

            render_pass.draw_indexed(0..self.pipeline.indices_count as u32, 0, 0..1);
        }

        queue.submit(std::iter::once(encoder.finish()));
    }
}
