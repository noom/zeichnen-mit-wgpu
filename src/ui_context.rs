use log::warn;

use imgui::{self, Context, FontConfig, FontSource, MouseCursor, TextureId};
use imgui_wgpu::Renderer;
use imgui_winit_support;

use winit::{self, event::Event, window::Window};

use wgpu::*;

use std::time::Instant;

use crate::{
    pipelines::{make_blit_pipeline, Pipeline},
    project::{Fields, Project},
    ui_controls::{create_controls, PropertiesState, UiEvent, UiState},
    ui_style::*,
    TEXTURE_FORMAT,
};

struct Preview {
    id: TextureId,
    pipeline: Pipeline,
}

pub struct UiContext {
    state: UiState,
    event: Option<UiEvent>,

    context: imgui::Context,
    platform: imgui_winit_support::WinitPlatform,
    renderer: Renderer,

    preview: Option<Preview>,

    last_frame: Instant,
    last_cursor: Option<MouseCursor>,
}

impl UiContext {
    pub fn new(window: &Window, device: &Device, queue: &Queue, scale_factor: f64) -> Self {
        let font_size = (13. * scale_factor) as f32;

        let state = UiState::default();
        // let state = UiState::from_project(project);

        let mut context = Context::create();
        context.set_ini_filename(None);
        context.io_mut().font_global_scale = (1. / scale_factor) as f32;

        let terminus_font = FontSource::TtfData {
            data: include_bytes!("../res/terminess_nerd_font_complete.ttf"),
            size_pixels: font_size,
            config: Some(FontConfig::default()),
        };

        context.fonts().add_font(&[terminus_font]);
        set_custom_style(context.style_mut());
        // context.style_mut().scale_all_sizes(scale_factor as f32);

        let mut platform = imgui_winit_support::WinitPlatform::init(&mut context);
        platform.attach_window(
            context.io_mut(),
            &window,
            imgui_winit_support::HiDpiMode::Default,
        );

        let renderer = Renderer::new(&mut context, device, queue, TEXTURE_FORMAT);

        Self {
            state,
            event: None,
            context,
            platform,
            renderer,
            preview: None,
            last_frame: Instant::now(),
            last_cursor: None,
        }
    }

    pub fn properties(&self) -> Option<&PropertiesState> {
        self.state.properties.as_ref()
    }

    pub fn event(&self) -> &Option<UiEvent> {
        &self.event
    }

    pub fn open_properties(&mut self) {
        self.state.properties = Some(PropertiesState::default());
    }

    pub fn close_properties(&mut self) {
        self.state.properties = None;
    }

    pub fn load_project(&mut self, project: &Fields) {
        self.state = UiState::from_project(project);
    }

    pub fn load_preview(
        &mut self,
        device: &Device,
        offscreen_view: &TextureView,
        resolution: &[u32; 2],
    ) {
        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label: Some("Preview texture"),
            size: wgpu::Extent3d {
                width: resolution[0],
                height: resolution[1],
                depth: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: TEXTURE_FORMAT,
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::SAMPLED,
        });
        let id = self.renderer.insert_texture(device, texture, None);

        let pipeline = make_blit_pipeline(&device, offscreen_view);

        self.preview = Some(Preview { id, pipeline });
    }

    pub fn resize_preview(
        &mut self,
        device: &Device,
        size: &[u32; 2],
        offscreen_view: &TextureView,
    ) {
        if let Some(preview) = &mut self.preview {
            let preview_texture = device.create_texture(&wgpu::TextureDescriptor {
                label: Some("Preview texture"),
                size: wgpu::Extent3d {
                    width: size[0],
                    height: size[1],
                    depth: 1,
                },
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: TEXTURE_FORMAT,
                usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::SAMPLED,
            });

            preview.pipeline = make_blit_pipeline(&device, &offscreen_view);

            self.renderer
                .replace_texture(preview.id, device, preview_texture, None);
        } else {
            warn!("UiContext::resize_preview was called while preview texture wasn't loaded");
        }
    }

    pub fn handle_event(&mut self, event: Event<()>, window: &Window) {
        self.platform
            .handle_event(self.context.io_mut(), &window, &event);
    }

    pub fn render(
        &mut self,
        project: Option<&Project>,
        window: &Window,
        device: &Device,
        queue: &Queue,
        encoder: &mut CommandEncoder,
        window_view: &TextureView,
    ) {
        self.blit_preview(encoder);

        self.context
            .io_mut()
            .update_delta_time(Instant::now().duration_since(self.last_frame));
        self.last_frame = Instant::now();

        self.platform
            .prepare_frame(self.context.io_mut(), &window)
            .expect("Failed to prepare frame");

        let ui = self.context.frame();

        let preview_texture_id = {
            if let Some(preview) = &self.preview {
                Some(preview.id)
            } else {
                None
            }
        };

        self.event = create_controls(
            &ui,
            &mut self.state,
            preview_texture_id,
            window.inner_size(),
            window.scale_factor(),
            project,
        );

        if self.last_cursor != ui.mouse_cursor() {
            self.last_cursor = ui.mouse_cursor();
            self.platform.prepare_render(&ui, &window);
        }

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: window_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            self.renderer
                .render(ui.render(), queue, device, &mut render_pass)
                .expect("Rendering failed");
        }
    }

    /// Blit preview texture only if preview texture is loaded; do nothing otherwise.
    fn blit_preview(&self, encoder: &mut CommandEncoder) {
        if let Some(preview) = &self.preview {
            let view = self
                .renderer
                .texture_view(preview.id)
                .expect("Couldn't retrieve preview texture view from id");
            let pipeline = &preview.pipeline;

            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&pipeline.render_pipeline);

            render_pass.set_bind_group(0, &pipeline.bind_group, &[]);

            render_pass.set_vertex_buffer(0, pipeline.vertex_buffer.slice(..));
            render_pass.set_index_buffer(pipeline.index_buffer.slice(..));

            render_pass.draw_indexed(0..pipeline.indices_count as u32, 0, 0..1);
        }
    }
}
