use nfd::Response;

use crate::{
    project::{ResourceId, SavingMethod},
    ui_controls::{MainEvent, PropertiesEvent, UiEvent},
    Application, Project, ShaderContext,
};

impl Application {
    fn handle_main_events(&mut self, event: MainEvent) {
        let window_mode = self.window_mode.as_mut().unwrap();
        let device = self.context.device();

        match event {
            MainEvent::CreateNewProject(name) => {
                let path = {
                    let result = nfd::open_pick_folder(None).expect("Nfd had an error");

                    match result {
                        Response::Okay(path) => path,
                        _ => return,
                    }
                };

                let mut project = Project::new(&name, &path);
                project.save(&SavingMethod::NewDirectory(path));

                self.project = Some(project);
                self.shader_context = None;
                window_mode.ui.open_properties();
            }
            MainEvent::LoadProject => {
                let path = {
                    let result = nfd::dialog()
                        .filter("zei")
                        .open()
                        .expect("Nfd had an error");

                    match result {
                        Response::Okay(path) => path,
                        _ => return,
                    }
                };

                let project = Project::from_path(path);

                window_mode.ui.load_project(&project.fields);

                if let Some(frag_shader_id) = &project.fields.fragment_shader {
                    let frag_shader_id = ResourceId::from(frag_shader_id);
                    let frag_shader_res = project
                        .resources()
                        .get(&frag_shader_id)
                        .expect("Shader wasn't correctly imported.");

                    let shader_context = ShaderContext::new(
                        device,
                        self.frame_rate,
                        frag_shader_res,
                        &project.fields.resolution,
                    );

                    window_mode.ui.load_preview(
                        device,
                        &shader_context.view(),
                        &project.fields.resolution,
                    );

                    self.shader_context = Some(shader_context);
                }

                self.project = Some(project);
            }
            MainEvent::SaveProjectTo => {
                let project = self
                    .project
                    .as_mut()
                    .expect("SaveProjectTo event received while no project is loaded");

                let path = {
                    let result = nfd::open_pick_folder(None).expect("Nfd had an error");

                    match result {
                        Response::Okay(path) => path,
                        _ => return,
                    }
                };

                project.save(&SavingMethod::NewDirectory(path));
            }
            MainEvent::SaveProject => {
                let project = self
                    .project
                    .as_mut()
                    .expect("SaveProject event received while no project is loaded");

                project.save(&SavingMethod::InPlace);
            }
        }
    }

    fn handle_properties_events(&mut self, event: PropertiesEvent) {
        let window_mode = self.window_mode.as_mut().unwrap();
        let device = self.context.device();

        match event {
            PropertiesEvent::ResolutionChanged(resolution) => {
                let project = self
                    .project
                    .as_mut()
                    .expect("ResolutionChanged event received while no project is loaded");

                project.fields.resolution = resolution.clone();

                if let Some(shader_context) = &mut self.shader_context {
                    shader_context.resize(device, &resolution);
                    window_mode
                        .ui
                        .resize_preview(device, &resolution, shader_context.view());
                }
            }
            PropertiesEvent::ImportShader => {
                let path = {
                    let result = nfd::dialog()
                        .filter("frag")
                        .open()
                        .expect("Nfd had an error");

                    match result {
                        Response::Okay(path) => path,
                        _ => return,
                    }
                };

                let project = self
                    .project
                    .as_mut()
                    .expect("ImportShader event received while no project is loaded");

                let imported_shader_res = {
                    let imported_shader_id = project.import_shader(&path);

                    project
                        .resources()
                        .get(&imported_shader_id)
                        .expect("Fragment shader resource not properly imported.")
                };

                if let Some(shader_context) = &mut self.shader_context {
                    shader_context.change_shader(device, imported_shader_res);
                } else {
                    let shader_context = ShaderContext::new(
                        &device,
                        self.frame_rate,
                        imported_shader_res,
                        &project.fields.resolution,
                    );

                    window_mode.ui.load_preview(
                        device,
                        shader_context.view(),
                        &project.fields.resolution,
                    );

                    self.shader_context = Some(shader_context);
                }
            }
            PropertiesEvent::ImportPng => unimplemented!(),
        }
    }

    pub fn handle_ui_events(&mut self) {
        let window_mode = if self.window_mode.is_some() {
            self.window_mode.as_mut().unwrap()
        } else {
            return;
        };

        if let Some(event) = window_mode.ui.event().clone() {
            match event {
                UiEvent::Main(event) => self.handle_main_events(event),
                UiEvent::Properties(event) => self.handle_properties_events(event),
            }
        }
    }
}
