use winit::{
    dpi::{PhysicalPosition, PhysicalSize},
    event::*,
    window::Window,
};

use wgpu::*;

use futures::executor::block_on;

use log::{info, warn};

use std::{
    fs::File,
    io::{Read, Write},
    path::Path,
};

mod handle_ui_events;

use crate::{
    context::Context, project::Project,
    shader_context::ShaderContext, ui_context::UiContext, ui_controls::UiEvent,
    window_context::WindowContext, HEADLESS_SIZE, TEXTURE_FORMAT,
};

struct WindowMode {
    context: WindowContext,
    ui: UiContext,
}

pub struct Application {
    pub(in crate::application) frame_rate: u32,

    pub(in crate::application) context: Context,

    pub(in crate::application) project: Option<Project>,

    pub(in crate::application) shader_context: Option<ShaderContext>,

    pub(in crate::application) window_mode: Option<WindowMode>,

    pub take_capture: bool,
    pub(in crate::application) capture_buffer: Option<(Buffer, BufferDimensions)>,

    pub(in crate::application) test_counter: f32,
}

impl Application {
    // Creating some of the wgpu types requires async code
    pub async fn new(frame_rate: u32, window: Option<Window>) -> Self {
        // The instance is a handle to our GPU
        // BackendBit::PRIMARY => Vulkan + Metal + DX12 + Browser WebGPU
        let instance = Instance::new(BackendBit::PRIMARY);

        let texture_format = TEXTURE_FORMAT;

        let size = if let Some(window) = &window {
            window.inner_size()
        } else {
            HEADLESS_SIZE
        };

        let context = Context::new(&instance, size, texture_format).await;

        let (device, queue, _size) = (context.device(), context.queue(), context.size());

        let window_mode = if let Some(window) = window {
            let context = WindowContext::new(&instance, window, device);
            let ui = UiContext::new(
                &context.window,
                device,
                queue,
                context.window.scale_factor(),
            );

            Some(WindowMode { context, ui })
        } else {
            None
        };

        Self {
            frame_rate,
            context,
            project: None,
            shader_context: None,
            window_mode,
            take_capture: false,
            capture_buffer: None,
            test_counter: 0.,
        }
    }

    pub fn event(&mut self, event: Event<()>) {
        if let Some(window_mode) = &mut self.window_mode {
            window_mode
                .ui
                .handle_event(event, &window_mode.context.window);
        }
    }

    // TODO: remove me
    // fn window_event(&mut self, event: &WindowEvent) {
    //     if let Some(window_mode) = &mut self.window_mode {
    //         window_mode.context.window_event(event);
    //     }
    // }

    pub fn resized(&mut self, new_size: &winit::dpi::PhysicalSize<u32>) {
        self.context.size = new_size.clone();

        if let Some(window_mode) = &mut self.window_mode {
            window_mode
                .context
                .resize_window(self.context.device(), new_size);
        }
    }

    pub fn scale_factor_changed(&mut self, new_inner_size: &PhysicalSize<u32>) {
        self.resized(new_inner_size);
    }

    pub fn cursor_moved(&mut self, position: &PhysicalPosition<f64>) {
        if let Some(shader_context) = &mut self.shader_context {
            shader_context.update_cursor_position(self.context.size(), position);
        }
    }

    // fn redraw_requested(&mut self) {
    pub fn redraw_events_cleared(&mut self) {
        self.update();
        self.render();

        if self.take_capture {
            block_on(self.create_png("CAPTURE.png"));
            self.take_capture = false;
        }
    }

    // TODO: am I useful?
    pub fn main_events_cleared(&mut self) {
        if let Some(_window_mode) = &mut self.window_mode {
            // window_mode.request_redraw();
        }
    }

    // TODO: am I useful?
    // pub fn load_project(&mut self) {
    //     // let shader_context = ShaderContext::new(&project, device, &texture_format, frame_rate);
    // }

    pub fn update(&mut self) {
        // TODO: fix your timestamps!
        // println!("{:?}\t{}", self.app_state.time, self.test_counter);
        self.test_counter += 1. / 60.;

        self.handle_ui_events();

        let (device, queue) = (self.context.device(), self.context.queue());

        if let Some(window_mode) = &self.window_mode {
            if let Some(properties_state) = window_mode.ui.properties() {
                if properties_state.run && self.shader_context.is_some() && self.project.is_some() {
                    let (shader_context, project) = (
                        self.shader_context.as_mut().unwrap(),
                        self.project.as_ref().unwrap(),
                    );

                    shader_context.render(&project, device, queue);
                }
            }
        }
    }

    pub fn render(&mut self) {
        let (device, queue, _size) = (
            self.context.device(),
            self.context.queue(),
            self.context.size(),
        );

        let mut encoder = device.create_command_encoder(&CommandEncoderDescriptor {
            label: Some("Render Encoder"),
        });

        if self.take_capture {}

        let frame = if let Some(window_mode) = &mut self.window_mode {
            let frame = window_mode
                .context
                .begin_render(device, queue, &mut encoder);

            if let Some(frame) = &frame {
                window_mode.ui.render(
                    self.project.as_ref(),
                    &window_mode.context.window,
                    device,
                    queue,
                    &mut encoder,
                    &frame.output.view,
                );
            }

            frame
        } else {
            None
        };

        queue.submit(std::iter::once(encoder.finish()));

        // Explicitly drop `frame` for clarity and to avoid "unused assignment warning".
        drop(frame);
    }

    fn take_capture(
        &mut self,
        device: &Device,
        size: &PhysicalSize<u32>,
        encoder: &mut CommandEncoder,
    ) {
        if let Some(shader_context) = &self.shader_context {
            let capture_buffer_dim =
                BufferDimensions::new(size.width as usize, size.height as usize);
            // The output buffer lets us retrieve the data as an array
            let capture_buffer = device.create_buffer(&BufferDescriptor {
                label: None,
                size: (capture_buffer_dim.padded_bytes_per_row * capture_buffer_dim.height) as u64,
                usage: BufferUsage::MAP_READ | BufferUsage::COPY_DST,
                mapped_at_creation: false,
            });

            // Copy the data from the texture to the buffer
            encoder.copy_texture_to_buffer(
                TextureCopyView {
                    texture: shader_context.texture(),
                    mip_level: 0,
                    origin: Origin3d::ZERO,
                },
                BufferCopyView {
                    buffer: &capture_buffer,
                    layout: TextureDataLayout {
                        offset: 0,
                        bytes_per_row: capture_buffer_dim.padded_bytes_per_row as u32,
                        rows_per_image: 0,
                    },
                },
                Extent3d {
                    width: capture_buffer_dim.width as u32,
                    height: capture_buffer_dim.height as u32,
                    depth: 1,
                },
            );

            self.capture_buffer = Some((capture_buffer, capture_buffer_dim));
        } else {
            warn!("Application::take_capture was called without shader_context being created");
        }
    }

    pub async fn create_png(&self, png_output_path: &str) {
        let (capture_buffer, buffer_dimensions) = self.capture_buffer.as_ref().unwrap();

        // Note that we're not calling `.await` here.
        let buffer_slice = capture_buffer.slice(..);
        let buffer_future = buffer_slice.map_async(MapMode::Read);

        // Poll the device in a blocking manner so that our future resolves.
        // In an actual application, `device.poll(...)` should
        // be called in an event loop or on another thread.
        self.context.device().poll(Maintain::Wait);
        // If a file system is available, write the buffer as a PNG
        let has_file_system_available = cfg!(not(target_arch = "wasm32"));
        if !has_file_system_available {
            return;
        }

        if let Ok(()) = buffer_future.await {
            let padded_buffer = buffer_slice.get_mapped_range();

            let mut png_encoder = png::Encoder::new(
                File::create(png_output_path).unwrap(),
                buffer_dimensions.width as u32,
                buffer_dimensions.height as u32,
            );
            png_encoder.set_depth(png::BitDepth::Eight);
            png_encoder.set_color(png::ColorType::RGBA);
            let mut png_writer = png_encoder
                .write_header()
                .unwrap()
                .into_stream_writer_with_size(buffer_dimensions.unpadded_bytes_per_row);

            // from the padded_buffer we write just the unpadded bytes into the image
            for chunk in padded_buffer.chunks(buffer_dimensions.padded_bytes_per_row) {
                png_writer
                    .write(&chunk[..buffer_dimensions.unpadded_bytes_per_row])
                    .unwrap();
            }
            png_writer.finish().unwrap();

            // With the current interface, we have to make sure all mapped views are
            // dropped before we unmap the buffer.
            drop(padded_buffer);

            capture_buffer.unmap();
        }
    }
}

struct BufferDimensions {
    width: usize,
    height: usize,
    unpadded_bytes_per_row: usize,
    padded_bytes_per_row: usize,
}

impl BufferDimensions {
    fn new(width: usize, height: usize) -> Self {
        let bytes_per_pixel = std::mem::size_of::<u32>();
        let unpadded_bytes_per_row = width * bytes_per_pixel;
        let align = COPY_BYTES_PER_ROW_ALIGNMENT as usize;
        let padded_bytes_per_row_padding = (align - unpadded_bytes_per_row % align) % align;
        let padded_bytes_per_row = unpadded_bytes_per_row + padded_bytes_per_row_padding;
        Self {
            width,
            height,
            unpadded_bytes_per_row,
            padded_bytes_per_row,
        }
    }
}
