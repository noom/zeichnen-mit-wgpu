use wgpu::{util::DeviceExt, *};

use glsl_layout::{self as glsl, as_bytes, AsStd140};

use std::{
    fs::File,
    io::Read,
    path::{Path, PathBuf},
};

use crate::{
    shader_context::FragmentUniforms,
    utils::{make_quad_index_buffer, make_quad_vertex_buffer, Vertex, QUAD_INDICES},
    TEXTURE_FORMAT,
};

pub struct Pipeline {
    pub render_pipeline: RenderPipeline,
    pub bind_group: BindGroup,
    pub fragment_uniforms_buffer: Option<Buffer>,
    pub vertex_buffer: Buffer,
    pub index_buffer: Buffer,
    pub indices_count: usize,
}

pub struct ShaderDescriptor<T> {
    src: String,
    uniforms: Option<T>,
}

// #[derive(Default)]
pub struct PipelineBuilder<T> {
    // pub render_pipeline: Option<RenderPipeline>,
    // pub bind_group: Option<BindGroup>,
    // pub uniforms_buffer: Option<Buffer>,
    // pub vertex_buffer: Option<Buffer>,
    // pub index_buffer: Option<Buffer>,
    // pub indices_count: Option<usize>,
    fs: Option<ShaderDescriptor<T>>,
    vs: Option<ShaderDescriptor<T>>,
}

impl<T: AsStd140> PipelineBuilder<T> {
    pub fn new() -> Self {
        Self { vs: None, fs: None }
    }

    // pub fn old_vertex_shader(&mut self, vs_src: impl Into<String>) -> &mut Self {
    //     self.vs_src = Some(vs_src.into());

    //     self
    // }

    // pub fn old_fragment_shader(&mut self, fs_src: impl Into<String>) -> &mut Self {
    //     self.fs_src = Some(fs_src.into());

    //     self
    // }

    pub fn vertex_shader(mut self, vs: ShaderDescriptor<T>) -> Self {
        self.vs = Some(vs);
        self
    }

    pub fn fragment_shader(mut self, fs: ShaderDescriptor<T>) -> Self {
        self.fs = Some(fs);
        self
    }

    pub fn build(self, device: &Device) -> Pipeline {
        let vs = self
            .vs
            .expect("No vertex shader provided to PipelineBuilder.");

        let fs = self
            .fs
            .expect("No fragment shader provided to PipelineBuilder.");

        let (vs_mod, fs_mod) = {
            let mut compiler = shaderc::Compiler::new().unwrap();
            let vs_spirv = compiler
                .compile_into_spirv(
                    &vs.src,
                    shaderc::ShaderKind::Vertex,
                    "shader.vert",
                    "main",
                    None,
                )
                .unwrap();
            let fs_spirv = compiler
                .compile_into_spirv(
                    &fs.src,
                    shaderc::ShaderKind::Fragment,
                    "shader.frag",
                    "main",
                    None,
                )
                .unwrap();

            let vs_mod =
                device.create_shader_module(wgpu::util::make_spirv(&vs_spirv.as_binary_u8()));
            let fs_mod =
                device.create_shader_module(wgpu::util::make_spirv(&fs_spirv.as_binary_u8()));

            (vs_mod, fs_mod)
        };

        let fragment_uniforms_buffer = if let Some(uniforms) = fs.uniforms {
            Some(
                device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some("Fragment Shader Uniforms Buffer"),
                    contents: as_bytes(&uniforms),
                    usage: BufferUsage::UNIFORM | BufferUsage::COPY_DST,
                }),
            )
        } else {
            None
        };

        let (bind_group_layout, bind_group) = {
            let mut bd_layout_entries = Vec::<BindGroupLayoutEntry>::new();
            let mut bd_entries = Vec::<BindGroupEntry>::new();

            if let Some(fragment_uniforms_buffer) = &fragment_uniforms_buffer {
                let uniforms_layout_entry = BindGroupLayoutEntry {
                    // TODO: Make binding increment it supports multiple uniforms
                    binding: 0,
                    visibility: ShaderStage::FRAGMENT,
                    ty: BindingType::UniformBuffer {
                        dynamic: false,
                        min_binding_size: None,
                    },
                    count: None,
                };

                let uniforms_entry = BindGroupEntry {
                    binding: 0,
                    resource: BindingResource::Buffer(fragment_uniforms_buffer.slice(..)),
                };

                bd_layout_entries.push(uniforms_layout_entry);
                bd_entries.push(uniforms_entry);
            }

            let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
                entries: &bd_layout_entries,
                label: Some("Production bind group layout"),
            });

            let bind_group = device.create_bind_group(&BindGroupDescriptor {
                layout: &bind_group_layout,
                entries: &bd_entries,
                label: Some("Production bind group"),
            });

            (bind_group_layout, bind_group)
        };

        let render_pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some("Production render pipeline layout"),
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });

        let render_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: Some("Production render pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex_stage: ProgrammableStageDescriptor {
                module: &vs_mod,
                entry_point: "main",
            },
            fragment_stage: Some(ProgrammableStageDescriptor {
                module: &fs_mod,
                entry_point: "main",
            }),
            rasterization_state: Some(RasterizationStateDescriptor {
                front_face: FrontFace::Ccw,
                cull_mode: CullMode::Back,
                clamp_depth: false,
                depth_bias: 0,
                depth_bias_slope_scale: 0.0,
                depth_bias_clamp: 0.0,
            }),
            color_states: &[ColorStateDescriptor {
                format: TEXTURE_FORMAT,
                color_blend: BlendDescriptor::REPLACE,
                alpha_blend: BlendDescriptor::REPLACE,
                write_mask: ColorWrite::ALL,
            }],
            primitive_topology: PrimitiveTopology::TriangleList,
            depth_stencil_state: None,
            vertex_state: VertexStateDescriptor {
                index_format: IndexFormat::Uint16,
                vertex_buffers: &[Vertex::desc()],
            },
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        });

        Pipeline {
            render_pipeline,
            bind_group,
            fragment_uniforms_buffer,
            vertex_buffer: make_quad_vertex_buffer(device),
            index_buffer: make_quad_index_buffer(device),
            indices_count: QUAD_INDICES.len(),
        }
    }
}

// pub fn make_shader_pipeline(device: &Device, frag_shader_path: impl AsRef<Path>) -> Pipeline {
pub fn make_shader_pipeline(device: &Device, frag_shader: &str) -> Pipeline {
    let vs_src = include_str!("shader.vert");

    let fs_src = frag_shader;

    let mut compiler = shaderc::Compiler::new().unwrap();
    let vs_spirv = compiler
        .compile_into_spirv(
            vs_src,
            shaderc::ShaderKind::Vertex,
            "shader.vert",
            "main",
            None,
        )
        .unwrap();
    let fs_spirv = compiler
        .compile_into_spirv(
            &fs_src,
            shaderc::ShaderKind::Fragment,
            "shader.frag",
            "main",
            None,
        )
        .unwrap();
    let vs_module = device.create_shader_module(wgpu::util::make_spirv(&vs_spirv.as_binary_u8()));
    let fs_module = device.create_shader_module(wgpu::util::make_spirv(&fs_spirv.as_binary_u8()));

    let uniforms = FragmentUniforms {
        time: 0.,
        mouse_position: [0.; 2].into(),
        resolution: [0.; 2].into(),
    }
    .std140();

    let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Uniform Buffer"),
        contents: as_bytes(&uniforms),
        usage: BufferUsage::UNIFORM | BufferUsage::COPY_DST,
    });

    let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
        entries: &[BindGroupLayoutEntry {
            binding: 0,
            visibility: ShaderStage::FRAGMENT,
            ty: BindingType::UniformBuffer {
                dynamic: false,
                min_binding_size: None,
            },
            count: None,
        }],
        label: Some("Production bind group layout"),
    });

    let bind_group = device.create_bind_group(&BindGroupDescriptor {
        layout: &bind_group_layout,
        entries: &[BindGroupEntry {
            binding: 0,
            resource: BindingResource::Buffer(uniform_buffer.slice(..)),
        }],
        label: Some("Production bind group"),
    });

    let render_pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
        label: Some("Production render pipeline layout"),
        bind_group_layouts: &[&bind_group_layout],
        push_constant_ranges: &[],
    });

    let render_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
        label: Some("Production render pipeline"),
        layout: Some(&render_pipeline_layout),
        vertex_stage: ProgrammableStageDescriptor {
            module: &vs_module,
            entry_point: "main",
        },
        fragment_stage: Some(ProgrammableStageDescriptor {
            module: &fs_module,
            entry_point: "main",
        }),
        rasterization_state: Some(RasterizationStateDescriptor {
            front_face: FrontFace::Ccw,
            cull_mode: CullMode::Back,
            clamp_depth: false,
            depth_bias: 0,
            depth_bias_slope_scale: 0.0,
            depth_bias_clamp: 0.0,
        }),
        color_states: &[ColorStateDescriptor {
            format: TEXTURE_FORMAT,
            color_blend: BlendDescriptor::REPLACE,
            alpha_blend: BlendDescriptor::REPLACE,
            write_mask: ColorWrite::ALL,
        }],
        primitive_topology: PrimitiveTopology::TriangleList,
        depth_stencil_state: None,
        vertex_state: VertexStateDescriptor {
            index_format: IndexFormat::Uint16,
            vertex_buffers: &[Vertex::desc()],
        },
        sample_count: 1,
        sample_mask: !0,
        alpha_to_coverage_enabled: false,
    });

    Pipeline {
        render_pipeline,
        bind_group,
        fragment_uniforms_buffer: Some(uniform_buffer),
        vertex_buffer: make_quad_vertex_buffer(device),
        index_buffer: make_quad_index_buffer(device),
        indices_count: QUAD_INDICES.len(),
    }
}

#[derive(AsStd140)]
struct NoUniforms {
    nothing_to_see: glsl::int,
}

// TODO: you were working on this before starting to procrastinate forever again
pub fn make_blit_pipeline_new(device: &Device, view: &TextureView) -> Pipeline {
    // let fs_src = {
    //     let fs_src = String::new();
    //     fs_src.remove
    //     include_str!("quad_texture.frag");
    // };

    let pipeline = PipelineBuilder::<NoUniforms>::new()
        .fragment_shader(ShaderDescriptor {
            src: include_str!("quad_texture.frag").into(),
            uniforms: None,
        })
        .vertex_shader(ShaderDescriptor {
            src: include_str!("quad_texture.vert").into(),
            uniforms: None,
        });

    pipeline.build(device)
}

pub fn make_blit_pipeline(device: &Device, view: &TextureView) -> Pipeline {
    let vs_src = include_str!("quad_texture.vert");
    let fs_src = include_str!("quad_texture.frag");
    let mut compiler = shaderc::Compiler::new().unwrap();
    let vs_spirv = compiler
        .compile_into_spirv(
            vs_src,
            shaderc::ShaderKind::Vertex,
            "quad_texture.vert",
            "main",
            None,
        )
        .unwrap();
    let fs_spirv = compiler
        .compile_into_spirv(
            fs_src,
            shaderc::ShaderKind::Fragment,
            "quad_texture.frag",
            "main",
            None,
        )
        .unwrap();
    let vs_module = device.create_shader_module(wgpu::util::make_spirv(&vs_spirv.as_binary_u8()));
    let fs_module = device.create_shader_module(wgpu::util::make_spirv(&fs_spirv.as_binary_u8()));

    // TODO: should we keep this in State?
    let offscreen_texture_sampler = device.create_sampler(&SamplerDescriptor {
        address_mode_u: AddressMode::ClampToEdge,
        address_mode_v: AddressMode::ClampToEdge,
        address_mode_w: AddressMode::ClampToEdge,
        mag_filter: FilterMode::Linear,
        min_filter: FilterMode::Nearest,
        mipmap_filter: FilterMode::Nearest,
        ..Default::default()
    });

    let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
        entries: &[
            BindGroupLayoutEntry {
                binding: 0,
                visibility: ShaderStage::FRAGMENT,
                ty: BindingType::SampledTexture {
                    multisampled: false,
                    dimension: TextureViewDimension::D2,
                    component_type: TextureComponentType::Uint,
                },
                count: None,
            },
            BindGroupLayoutEntry {
                binding: 1,
                visibility: ShaderStage::FRAGMENT,
                ty: BindingType::Sampler { comparison: false },
                count: None,
            },
        ],
        label: Some("Blit bind group layout"),
    });

    let bind_group = device.create_bind_group(&BindGroupDescriptor {
        layout: &bind_group_layout,
        entries: &[
            BindGroupEntry {
                binding: 0,
                resource: BindingResource::TextureView(view),
            },
            BindGroupEntry {
                binding: 1,
                resource: BindingResource::Sampler(&offscreen_texture_sampler),
            },
        ],
        label: Some("Blit bind group"),
    });

    let render_pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
        label: Some("Blit render pipeline layout"),
        bind_group_layouts: &[&bind_group_layout],
        push_constant_ranges: &[],
    });

    let render_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
        label: Some("Blit render pipeline"),
        layout: Some(&render_pipeline_layout),
        vertex_stage: ProgrammableStageDescriptor {
            module: &vs_module,
            entry_point: "main",
        },
        fragment_stage: Some(ProgrammableStageDescriptor {
            module: &fs_module,
            entry_point: "main",
        }),
        rasterization_state: Some(RasterizationStateDescriptor {
            front_face: FrontFace::Ccw,
            cull_mode: CullMode::Back,
            clamp_depth: false,
            depth_bias: 0,
            depth_bias_slope_scale: 0.0,
            depth_bias_clamp: 0.0,
        }),
        color_states: &[ColorStateDescriptor {
            format: TEXTURE_FORMAT,
            color_blend: BlendDescriptor::REPLACE,
            alpha_blend: BlendDescriptor::REPLACE,
            write_mask: ColorWrite::ALL,
        }],
        primitive_topology: PrimitiveTopology::TriangleList,
        depth_stencil_state: None,
        vertex_state: VertexStateDescriptor {
            index_format: IndexFormat::Uint16,
            vertex_buffers: &[Vertex::desc()],
        },
        sample_count: 1,
        sample_mask: !0,
        alpha_to_coverage_enabled: false,
    });

    Pipeline {
        render_pipeline,
        bind_group,
        fragment_uniforms_buffer: None,
        vertex_buffer: make_quad_vertex_buffer(device),
        index_buffer: make_quad_index_buffer(device),
        indices_count: QUAD_INDICES.len(),
    }
}
