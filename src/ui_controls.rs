use winit::dpi::PhysicalSize;

use imgui::*;

use std::sync::mpsc::{channel, Sender};

use crate::project::{Fields, Project};

fn make_buf(text: &str, capacity: usize) -> ImString {
    let mut string = String::with_capacity(capacity);
    string.push_str(text);

    ImString::new(string)
}

#[derive(Debug, Clone)]
pub enum PropertiesEvent {
    ResolutionChanged([u32; 2]),
    ImportShader,
    ImportPng,
}

#[derive(Debug, Clone)]
pub enum MainEvent {
    CreateNewProject(String),
    LoadProject,
    SaveProjectTo,
    SaveProject,
}

#[derive(Debug, Clone)]
pub enum UiEvent {
    /// Events related to ui elements created in `main_controls()`.
    Main(MainEvent),
    /// Events related to ui elements created in `properties_controls()`.
    Properties(PropertiesEvent),
}

pub struct MainState {
    pub new_project_name: ImString,
}

impl Default for MainState {
    fn default() -> Self {
        Self {
            new_project_name: make_buf("", 15),
        }
    }
}

pub struct PropertiesState {
    pub run: bool,
    pub resolution: [i32; 2],
}

impl Default for PropertiesState {
    fn default() -> Self {
        Self {
            run: true,
            resolution: [800, 600],
        }
    }
}

pub struct UiState {
    pub main: MainState,
    pub properties: Option<PropertiesState>,
}

impl UiState {
    pub fn from_project(project: &Fields) -> Self {
        let properties = PropertiesState {
            resolution: [project.resolution[0] as i32, project.resolution[1] as i32],
            ..Default::default()
        };

        Self {
            properties: Some(properties),
            ..Default::default()
        }
    }
}

impl Default for UiState {
    fn default() -> Self {
        Self {
            properties: None,
            main: Default::default(),
        }
    }
}

fn main_controls(ui: &Ui, state: &mut MainState, project_loaded: bool, tx: Sender<UiEvent>) {
    ui.popup_modal(im_str!("New project"))
        .resizable(false)
        .build(|| {
            ui.input_text(im_str!("name"), &mut state.new_project_name)
                .build();

            if ui.small_button(im_str!("cancel")) {
                ui.close_current_popup();
            }

            ui.same_line(0.);

            if ui.small_button(im_str!("create new project")) {
                ui.close_current_popup();

                tx.send(UiEvent::Main(MainEvent::CreateNewProject(
                    state.new_project_name.to_string(),
                )))
                .unwrap();
            }
        });
    let mut open_new_project_popup = false;

    ui.main_menu_bar(|| {
        ui.menu(im_str!("File"), true, || {
            if MenuItem::new(im_str!("New project ...")).build(ui) {
                open_new_project_popup = true;
            } else if MenuItem::new(im_str!("Load project ...")).build(ui) {
                tx.send(UiEvent::Main(MainEvent::LoadProject)).unwrap();
            } else if MenuItem::new(im_str!("Save project to ..."))
                .enabled(project_loaded)
                .build(ui)
            {
                tx.send(UiEvent::Main(MainEvent::SaveProjectTo)).unwrap();
            } else if MenuItem::new(im_str!("Save project"))
                .enabled(project_loaded)
                .build(ui)
            {
                tx.send(UiEvent::Main(MainEvent::SaveProject)).unwrap();
            }
        });
    });

    if open_new_project_popup {
        ui.open_popup(im_str!("New project"));
    }
}

fn properties_controls(
    ui: &Ui,
    state: &mut PropertiesState,
    project_name: &str,
    preview_texture_id: Option<TextureId>,
    tx: Sender<UiEvent>,
) {
    let window_label = ImString::new(format!("Zeichnen - {}###Properties", project_name));

    Window::new(&window_label)
        .size([300., 150.], Condition::FirstUseEver)
        .build(&ui, || {
            ui.checkbox(im_str!("run"), &mut state.run);

            if ui
                .input_int2(im_str!("resolution"), &mut state.resolution)
                .build()
            {
                if state.resolution[0] < 0 {
                    state.resolution[0] = 0;
                } else if state.resolution[0] > 1920 {
                    state.resolution[0] = 1920;
                }
                if state.resolution[1] < 0 {
                    state.resolution[1] = 0;
                } else if state.resolution[1] > 1080 {
                    state.resolution[1] = 1080;
                }
            }
            if ui.is_item_deactivated_after_edit() {
                tx.send(UiEvent::Properties(PropertiesEvent::ResolutionChanged([
                    state.resolution[0] as u32,
                    state.resolution[1] as u32,
                ])))
                .unwrap();
            }

            if ui.small_button(im_str!("import a fragment shader")) {
                tx.send(UiEvent::Properties(PropertiesEvent::ImportShader))
                    .unwrap();
            }

            if ui.small_button(im_str!("import a png file")) {
                tx.send(UiEvent::Properties(PropertiesEvent::ImportPng))
                    .unwrap();
            }
        });

    Window::new(im_str!("Preview"))
        .position([250., 100.], Condition::FirstUseEver)
        .size([150., 150.], Condition::FirstUseEver)
        .no_decoration()
        .resizable(true)
        .build(&ui, || {
            if let Some(preview_texture_id) = preview_texture_id {
                let size = ui.content_region_avail();
                Image::new(preview_texture_id, size).build(ui);
            }
        });
}

pub fn create_controls(
    ui: &Ui,
    state: &mut UiState,
    preview_texture_id: Option<TextureId>,
    window_size: PhysicalSize<u32>,
    window_scale_factor: f64,
    project: Option<&Project>,
) -> Option<UiEvent> {
    // NOTE: The following calculation fix imgui's window size, I have no idea why though.
    // NOTE: Try with `1. / scale_factor` as you do for fonts in ui_context.rs you dumbass
    let window_scale_factor = (window_scale_factor as f32 * 10.).trunc() / 10. - 1.;
    let _scaled_window_size = [
        window_size.width as f32 * (window_scale_factor),
        window_size.height as f32 * (window_scale_factor),
    ];

    let (tx, rx) = channel::<UiEvent>();

    main_controls(ui, &mut state.main, project.is_some(), tx.clone());

    if let Some(properties_state) = &mut state.properties {
        let project_name = &project
            .expect("Project wasn't loaded when opening properties window")
            .info
            .name;

        properties_controls(
            ui,
            properties_state,
            project_name,
            preview_texture_id,
            tx.clone(),
        );
    }

    match rx.try_recv() {
        Ok(event) => Some(event),
        Err(_) => None,
    }
}
